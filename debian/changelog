tf5 (5.0beta8-14) unstable; urgency=medium

  * Fix compilation errors with GCC 14, one of which was introduced with
    the port to PCRE2 and the other of which is a long-standing issue in
    the upstream source. (Closes: #1075568)
  * Migrate to debputy.
  * Add X-Style: black to debian/control and reformat with debputy.

 -- Russ Allbery <rra@debian.org>  Thu, 25 Jul 2024 19:54:06 -0700

tf5 (5.0beta8-13) unstable; urgency=medium

  * Add README.source explaining the maintainer workflow for tf5.
  * Remove the unneeded debian/patches/* directory from the Git
    development branch.  The package is now maintained using dgit's
    split-view feature, which avoids committing the redundant
    debian/patches/* files.
  * Update standards version to 4.7.0 (no changes required).
  * Remove obsolete Lintian override for GPL code linked with OpenSSL.

 -- Russ Allbery <rra@debian.org>  Fri, 28 Jun 2024 10:18:48 -0700

tf5 (5.0beta8-12) unstable; urgency=medium

  * Fix name of --disable-symlink option passed to configure to avoid
    creating a spurious src/default symlink, which prevented tf5 from
    building again after debian/rules binary; debian/rules clean.
    (Closes: #1045709)
  * Convert to dh_installalternatives instead of manually-managed
    maintainer scripts.

 -- Russ Allbery <rra@debian.org>  Sun, 22 Oct 2023 11:19:24 -0700

tf5 (5.0beta8-11) unstable; urgency=medium

  * Port to PCRE2.  (Closes: #999943)
  * Build-depend on libncurses-dev instead of libncurses5-dev.
  * Drop build dependency on autoconf, since this is now part of the
    debhelper dependencies and the package build does not call autoconf
    directly, only via the standard debhelper tool.
  * Update lintian overrides for renamed tags and new syntax.
  * Update standards version to 4.6.2 (no changes required).

 -- Russ Allbery <rra@debian.org>  Mon, 17 Jul 2023 20:48:57 -0700

tf5 (5.0beta8-10) unstable; urgency=medium

  * Add patch to bring the main_loop function declaration into scope to
    suppress a warning from the build log scanner.
  * Add patch to pass LDFLAGS to the compiler when building makehelp.
    This binary isn't installed, so this doesn't really matter, but it
    suppresses a warning from the build log scanner.
  * Change the Debian packaging branch to debian/unstable.
  * Change canonical packaging repository to Salsa.
  * Update standards version to 4.5.1 (no changes required).

 -- Russ Allbery <rra@debian.org>  Sat, 26 Dec 2020 13:32:39 -0800

tf5 (5.0beta8-9) unstable; urgency=medium

  * Build against OpenSSL instead of GnuTLS, given the new ftp-master
    policy that OpenSSL can be treated as a system library and thus
    GPL-licensed software may be linked with it.  (Closes: #947734)
  * Add lintian overrides for possible-gpl-code-linked-with-openssl and
    for tags asking for various things to be forwarded to upstream (since
    upstream is quite dead).

 -- Russ Allbery <rra@debian.org>  Sat, 14 Nov 2020 16:37:34 -0800

tf5 (5.0beta8-8) unstable; urgency=medium

  * Fix FTBFS with GCC 10.  (Closes: #957873)
  * Fix spelling error in man page, caught by Lintian.
  * Convert the source package to a dgit debrebase workflow.
  * Update debhelper compatibility level to V13.
    - Depend on debhelper-compat instead of using debian/compat.
  * Update standards version to 4.5.0.
    - Add Rules-Requires-Root: no.
  * Add override for package-contains-documentation-outside-usr-share-doc.
    /usr/share/tf5/tf-lib/README documents the contents of that directory.

 -- Russ Allbery <rra@debian.org>  Sun, 10 May 2020 16:05:27 -0700

tf5 (5.0beta8-7) unstable; urgency=medium

  * Update debhelper compatibility level to V11.
    - Use dh_autoreconf but disable running autoheader.
  * Update standards version to 4.1.4.
    - Use https for Format URL in debian/copyright.
  * Update debian/watch to version 4 and use https.

 -- Russ Allbery <rra@debian.org>  Sat, 26 May 2018 19:03:58 -0700

tf5 (5.0beta8-6) unstable; urgency=medium

  * Apply patch from Chris Lamb for reproducible build.  (Closes: #834276)
  * Add a desktop file, although set it to NoDisplay since TinyFugue lacks
    an icon and it's not clear people would start it from the window
    manager menus.  (I considered making an icon from the graphic on the
    TinyFugue home page, but the licensing is unclear.)
  * Remove menu file in favor of the new desktop file.
  * Fix various spelling errors in the binary, caught by Lintian.
  * Enable all hardening flags.
  * Switch to separate Debian patches for each upstream change, managed
    using gbp pq.
  * Switch to the DEP-14 branch layout and add debian/gbp.conf reflecting
    that layout.  Update Vcs-Git accordingly.
  * Use HTTPS for Vcs-Git and Vcs-Browser URLs.
  * Fix duplicated license paragraph in debian/copyright.
  * Run wrap-and-sort -ast on the packaging files.
  * Remove explicit setting for xz compression, which is now the default.
  * Update standards version to 3.9.8.

 -- Russ Allbery <rra@debian.org>  Sun, 14 Aug 2016 14:27:28 -0700

tf5 (5.0beta8-5) unstable; urgency=low

  * Change build dependency to libgnutls-openssl-dev to assist with a
    GnuTLS transition.  (Closes: #731558)
  * Update debhelper compatibility level to V9.
    - Enable hardening flags, including bindnow and PIE.
  * Update watch file based on a revision by Bart Martens to use the new
    path to the SourceForge redirector.  Add some comments about choices
    of mangling and file extension matching.
  * Install upstream CREDITS in the package documentation directory.
  * Update standards version to 3.9.5.
    - Rewrite debian/copyright in copyright-format 1.0.
  * Convert to Debian source package format 3.0 (quilt) with a single
    merged Debian patch and use xz compression for the *.debian.tar.xz
    file.
  * Add information about SSL/TLS and (lack of) Unicode support to the
    package long description.
  * Fix a spelling error in the man page caught by Lintian.
  * Update Vcs-Git URL and add Vcs-Browser control field.

 -- Russ Allbery <rra@debian.org>  Sat, 07 Dec 2013 13:10:14 -0800

tf5 (5.0beta8-4) unstable; urgency=low

  * Update debhelper compatibility level to V7.
    - Use debhelper rule minimization with overrides.
    - List extra files to remove in debian/clean.
    - Install README via debian/docs instead of dh_installdocs argument.
    - Add ${misc:Depends} dependency.
  * Remove the tf5. prefix from packaging files.  We only generate one
    binary package and will for the forseeable future, so no need for the
    package prefix.
  * Remove old conflict on the pre-alternatives version of tf, which was
    older than what released with oldstable.
  * Update standards version to 3.8.3 (no changes required).

 -- Russ Allbery <rra@debian.org>  Mon, 24 Aug 2009 16:59:34 -0700

tf5 (5.0beta8-3) unstable; urgency=low

  * Build with SSL support using the GnuTLS OpenSSL compatibility layer.
    (Linking against OpenSSL directly isn't allowed since TinyFugue is
    under the GPL without an exception.)
  * Remove all modified and created files in debian/rules clean.  The
    upstream distclean wasn't comprehensive.
  * Update standards version to 3.8.0 (no changes required).

 -- Russ Allbery <rra@debian.org>  Mon, 30 Jun 2008 20:03:05 -0700

tf5 (5.0beta8-2) unstable; urgency=low

  * Correct the patch for format handling on 64-bit platforms.  Patch
    from JP Sugarbroad.  (Closes: #478602)

 -- Russ Allbery <rra@debian.org>  Tue, 29 Apr 2008 17:44:10 -0700

tf5 (5.0beta8-1) unstable; urgency=low

  * New maintainer.  (Closes: #465935)
  * New upstream release.  (Closes: #457011)
    - Incorporated all Debian bug fix patches except DESTDIR support.
  * Apply bug fix from upstream tracker for format handling that mostly
    affects 64-bit platforms.
  * Revert several groff-specific modifications to the man page to keep it
    closer to the upstream version.
  * Overhaul the build system.
    - Stop using CDBS and its patch system.  (Closes: #424205)
    - Stop including detached debugging symbols.  Most packages don't do
      this and I don't think it's worth the disk space.
    - Pass configure the right options for cross builds.
  * Change package section to net, matching the installation path, and
    mention chatservers in the package description.
  * Add a menu file.
  * Add a watch file.
  * Add Homepage and Vcs-Git control fields.
  * Remove extraneous comments in postinst and prerm and run both in more
    situations, not only configure and remove.
  * Clean up build dependencies.
  * Rewrite debian/copyright using my standard format and include
    additional upstream credits and the PCRE license (included upstream
    but not used for Debian builds).
  * Update standards version to 3.7.3 (no changes required).
  * Update debhelper compatibility level to V5 (no changes required).

 -- Russ Allbery <rra@debian.org>  Mon, 18 Feb 2008 10:16:32 -0800

tf5 (5.0beta7-2) unstable; urgency=low

  * Tell dh_strip to preserve debugging symbols stripped from the
    executable image so that they can be used via GDB's readlink
    mechanism (and, of course, ship them).
  * Added patch 002_strstr_needle_in_haystack, which fixes the 'calling
    replace with an empty match pattern causes an infinite loop'
    problem.
  * Cleaned up manpage to comply with modern (Unicode-aware) groff
    guidelines.

 -- Joel Aelwyn <fenton@debian.org>  Mon, 17 Oct 2005 14:04:56 -0600

tf5 (5.0beta7-1) unstable; urgency=low

  * New upstream version.
    - Build with the system PCRE library now that doing so is supported.
    - Updated patches (several no longer needed, the rest all required
      significant modifications).
    - Update description to mention (the new) 256-color terminal support.
    - Upstream removed the manpage; import a copy of the last known state
      from version 5.0b6, rather than have Yet Another Manpage-less Program
      in Debian.
  * Updated to Standards-Version 3.6.2.0 (no changes)
  * Incorporate a fix for the /fg command on 64-bit systems by Zephaniah
    E. Hull (Closes: #327448)

 -- Joel Aelwyn <fenton@debian.org>  Tue, 20 Sep 2005 02:34:31 -0600

tf5 (5.0beta6-4) experimental; urgency=low

  * Added a Suggests on the 'spell' package, which provides a binary
    compatible with the spell.tf module.

 -- Joel Aelwyn <fenton@debian.org>  Mon, 31 Jan 2005 15:49:13 -0700

tf5 (5.0beta6-3) experimental; urgency=low

  * Disable OpenSSL support until Ken Keys either includes a license
    exception for linking GPL and OpenSSL code, or support for GNU TLS
    is available. Unfortunately, the OpenSSL emulation layer in GNU TLS
    isn't sufficient to handle what TF uses. (No bug)
  * Fixed alternatives handling to include manpage as a slave link, and
    moved manpage to tf5.1.gz natively. (No bug)
  * Added patch to fix SEGV fault in strip_attr() function. (No bug yet;
    filed request with upstream for proper fix)
  * Updated to Standards-Version 3.6.1.0 (no changes)

 -- Joel Aelwyn <fenton@debian.org>  Tue, 25 Jan 2005 13:52:49 -0700

tf5 (5.0beta6-2) experimental; urgency=low

  * Added alternatives management for /usr/bin/tf, priority 15 (below
    'tf', which is the current stable release). (No bug)

 -- Joel Aelwyn <fenton@debian.org>  Tue,  4 Jan 2005 14:28:37 -0700

tf5 (5.0beta6-1) experimental; urgency=low

  * Initial packaging (Closes: #265974)

 -- Joel Aelwyn <fenton@debian.org>  Fri, 20 Aug 2004 14:05:11 -0600
